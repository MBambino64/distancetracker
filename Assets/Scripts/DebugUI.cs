﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DebugUI : MonoBehaviour {

	public TextMeshProUGUI txtLat;
	public TextMeshProUGUI txtLon;
	public TextMeshProUGUI txtAlt;
	public TextMeshProUGUI txtDis;

	public Button btnSetPos1;
	public Button btnSetPos2;

	float dis = 0f;
	LocationInfo pos1;
	LocationInfo pos2;

	void OnEnable() {
		btnSetPos1.onClick.AddListener(SetPos1);
		btnSetPos2.onClick.AddListener(SetPos2);
	}

	void OnDisable() {
		btnSetPos1.onClick.RemoveListener(SetPos1);
		btnSetPos2.onClick.RemoveListener(SetPos2);
	}

	public void UpdateUI() {
		LocationManager locMan = LocationManager.Instance;
		LocationInfo loc = locMan.GetLocation();

		txtLat.text = loc.latitude.ToString();
		txtLon.text = loc.longitude.ToString();
		txtAlt.text = loc.altitude.ToString();
		txtDis.text = dis.ToString() + "m";
	}

	void Update() {
		LocationManager locMan = LocationManager.Instance;
		dis = LocationManager.GetDistance(pos1, pos2);

		UpdateUI();
	}

	void SetPos1() {
		pos1 = LocationManager.Instance.GetLocation();
		print("Location 1 set!");
	}

	void SetPos2() {
		pos2 = LocationManager.Instance.GetLocation();
		print("Location 2 set!");
	}
}
