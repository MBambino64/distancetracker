﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthManager : MonoBehaviour {

	public Transform Marker;

	void Update() {
		LocationManager locMan = LocationManager.Instance;
		LocationInfo loc = locMan.GetLocation();

		transform.Rotate(0, 20 * Time.deltaTime, 0);
		Marker.transform.localPosition = LocationManager.LocationToSphere(loc, 0.5f);
	}
}
