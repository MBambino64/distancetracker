﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationManager : MonoBehaviour {

	const float EARTH_RADIUS = 6.371f * 1000000;

	static LocationManager _inst;
	public static LocationManager Instance {
		get {
			return _inst;
		}
	}

	bool _locationIsEnabled = false;
	LocationInfo startLocation;

	// public Vec
	public enum LocationStatus {
		Success,
		Timeout,
		Disabled,
		AlreadyOn,
		AlreadyOff,
		Failed
	}

	void Awake() {
		if (_inst == null) {
			_inst = this;
		} else {
			Destroy(gameObject);
			print("Cannot re-initialize Location Manager!!!");
		}
	}

	// Use this for initialization
	void Start() {
		StartCoroutine(InitLocation());
	}

	IEnumerator InitLocation() {
		if (_locationIsEnabled) {
			InitFailed(LocationStatus.AlreadyOn);
			yield break;
		}

		if (!Input.location.isEnabledByUser) {
			InitFailed(LocationStatus.Disabled);
			yield break;
		}

		Input.location.Start(5f, 1f);

		int maxWait = 20;
		while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0) {
			yield return new WaitForSeconds(1);
			maxWait--;
		}

		if (maxWait < 1) {
			InitFailed(LocationStatus.Timeout);
			yield break;
		}

		if (Input.location.status == LocationServiceStatus.Failed) {
			InitFailed(LocationStatus.Failed);
			yield break;
		} else {
			InitSuccess(LocationStatus.Success);
		}
	}

	void InitFailed(LocationStatus status) {
		_locationIsEnabled = false;
		print("Failed! Reason: " + status.ToString());
	}

	void InitSuccess(LocationStatus status) {
		_locationIsEnabled = true;
		startLocation = Input.location.lastData;
		print(startLocation);
	}

	public static Vector3 LocationToSphere(LocationInfo loc, float magnitude = 1f) {
		return new Vector3(
			Mathf.Cos(loc.longitude * Mathf.Deg2Rad) * Mathf.Cos(loc.latitude * Mathf.Deg2Rad),
			Mathf.Sin(loc.latitude * Mathf.Deg2Rad),
			Mathf.Sin(loc.longitude * Mathf.Deg2Rad) * Mathf.Cos(loc.latitude * Mathf.Deg2Rad)
		).normalized * magnitude;
	}

	public static float GetDistance(LocationInfo pos1, LocationInfo pos2) {
		Vector3 p1 = LocationToSphere(pos1, EARTH_RADIUS);
		Vector3 p2 = LocationToSphere(pos2, EARTH_RADIUS);

		return Vector3.Distance(p1, p2);
	}

	public LocationInfo GetLocation() {
		return Input.location.lastData;
	}

	public float DistanceFromStart(LocationInfo loc) {
		return GetDistance(startLocation, loc);
	}
}
